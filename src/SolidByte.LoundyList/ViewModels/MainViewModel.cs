﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SolidByte.LoundyList.ViewModels
{
    public class MainViewModel : ViewModel
    {
        private ObservableCollection<SelectableItem> itemsList;
        
        private string engineSvcAcc;
        private string engineSvcPw;
        private string agentSvcAcc;
        private string agentSvcPw;
        private string instanceName;
        private string installDirectory;
        private string dataFileDirectory;
        private string logFileDirectory;
        private string tempFileDirectory;
        private string backupFileDirectory;
        private string installType;
        private string sqlBinariesLocation;
        private string installSharedDir;
        private string installSharedWowDir;
        private string pid;
        private string failoverClusterDisks;
        private string failoverClusterGroup;
        private string failoverClusterIP;
        private string failoverClusterName;

        public MainViewModel()
        {
            this.InitializeItems();
            this.InitializeCommands();
        }

        public ICommand CreateOutputCommand { get; private set; }

        public string EngineSvcAcc
        {
            get { return this.engineSvcAcc; }
            set
            {
                this.engineSvcAcc = value;
                this.OnPropertyChanged();
            }
        }

        public string EngineSvcPw
        {
            get { return this.engineSvcPw; }
            set
            {
                this.engineSvcPw = value;
                this.OnPropertyChanged();
            }
        }

        public string AgentSvcAcc
        {
            get { return this.agentSvcAcc; }
            set
            {
                this.agentSvcAcc = value;
                this.OnPropertyChanged();
            }
        }

        public string AgentSvcPw
        {
            get { return this.agentSvcPw; }
            set
            {
                this.agentSvcPw = value;
                this.OnPropertyChanged();
            }
        }

        public string InstallDirectory
        {
            get { return this.installDirectory; }
            set
            {
                this.installDirectory = value;
                this.OnPropertyChanged();
            }
        }

        public string DataFileDirectory
        {
            get { return this.dataFileDirectory; }
            set
            {
                this.dataFileDirectory = value;
                this.OnPropertyChanged();
            }
        }

        public string LogFileDirectory
        {
            get { return this.logFileDirectory; }
            set
            {
                this.logFileDirectory = value;
                this.OnPropertyChanged();
            }
        }

        public string TempFileDirectory
        {
            get { return this.tempFileDirectory; }
            set
            {
                this.tempFileDirectory = value;
                this.OnPropertyChanged();
            }
        }

        public string BackupFileDirectory
        {
            get { return this.backupFileDirectory; }
            set
            {
                this.backupFileDirectory = value;
                this.OnPropertyChanged();
            }
        }

        public string InstanceName
        {
            get { return this.instanceName; }
            set
            {
                this.instanceName = value;
                this.OnPropertyChanged();
            }
        }

        public string InstallType
        {
            get { return this.installType; }
            set 
            {
                this.installType = value;
                this.OnPropertyChanged();
            }
        }

        public IEnumerable<SelectableItem> ItemsList
        {
            get { return this.itemsList; }
        }

        public string SqlBinariesLocation
        {
            get { return this.sqlBinariesLocation; }
            set
            {
                this.sqlBinariesLocation = value;
                this.OnPropertyChanged();
            }
        }

        public string InstallSharedDir
        {
            get { return this.installSharedDir; }
            set
            {
                this.installSharedDir = value;
                this.OnPropertyChanged();
            }
        }

        public string InstallSharedWowDir
        {
            get { return this.installSharedWowDir; }
            set
            {
                this.installSharedWowDir = value;
                this.OnPropertyChanged();
            }
        }

        public string Pid
        {
            get { return this.pid; }
            set
            {
                this.pid = value;
                this.OnPropertyChanged();
            }
        }

        public string FailoverClusterDisks
        {
            get { return this.failoverClusterDisks; }
            set
            {
                this.failoverClusterDisks = value;
                this.OnPropertyChanged();
            }
        }

        public string FailoverClusterGroup
        {
            get { return this.failoverClusterGroup; }
            set
            {
                this.failoverClusterGroup = value;
                this.OnPropertyChanged();
            }
        }

        public string FailoverClusterIP
        {
            get { return this.failoverClusterIP; }
            set
            {
                this.failoverClusterIP = value;
                this.OnPropertyChanged();
            }
        }

        public string FailoverClusterName
        {
            get { return this.failoverClusterName; }
            set
            {
                this.failoverClusterName = value;
                this.OnPropertyChanged();
            }
        }




        private void InitializeCommands()
        {
            this.CreateOutputCommand = new DelegateCommand(this.CreateOutput);
        }

        private void InitializeItems()
        {
            // items for the sql feature list

            // TODO: Put in a resource file

            var items = new[]
            {
                //Pre SQL 2012
                //"SQL_Engine",
                //"SQL_Data_Files",
                //"SQL_Replication",
                //"SQL_FullText",
                //"Client_Components",
                //"Connectivity",
                //"SQL_Tools90",
                //"SQLXML",
                //"Tools_Legacy",
                //"SQL_DTS",
                //"SQL_WarehouseDevWorkbench",

                "SQLENGINE",
                "REPLICATION",
                "FULLTEXT",
                "DQ",
                "AS",
                "RS",
                "RS_SHP",
                "RS_SHPWFE",
                "DQC",
                "BIDS",
                "CONN",
                "IS",
                "BC",
                "SDK",
                "BOL",
                "SSMS",
                "ADV_SSMS",
                "DREPLAY_CTLR",
                "DREPLAY_CLT",
                "MDS",
            };

            this.itemsList = new ObservableCollection<SelectableItem>(
                items.Select(x => new SelectableItem(x)));
        }

        private void CreateOutput()
        {
            var featureList = CreateFeatureList();

            var template = new TemplateBuilder();

            // not sure if this is a good place for this kind of logic?
            if (this.instanceName != null)
            {
                this.instanceName = this.instanceName.Trim();
            }
            
            if (this.instanceName==null || this.instanceName=="")
            {
                this.instanceName = "MSSQLSERVER";
            }

            if ((this.instanceName==null || this.instanceName=="" || this.instanceName=="MSSQLSERVER") 
                    && (this.engineSvcAcc==null || this.engineSvcAcc==""))
            {
                this.engineSvcAcc = "NT SERVICE\\MSSQLSERVER";
                this.AgentSvcAcc = "NT SERVICE\\SQLSERVERAGENT";
            }
            else if ((this.instanceName!=null || this.instanceName!="" || this.instanceName!="MSSQLSERVER") 
                && (this.engineSvcAcc==null || this.engineSvcAcc==""))
            {
                this.engineSvcAcc = "NT Service\\MSSQL$" + this.instanceName;
                this.agentSvcAcc = "NT Service\\SQLAgent$" + this.instanceName;
            }

            var parameters = new TemplateParameters()
            {
                InstanceName = this.instanceName,
                FeatureList = featureList,
                BackupFileDirectory = this.BackupFileDirectory,
                InstallDirectory = this.installDirectory,
                DataFileDirectory = this.DataFileDirectory,
                LogFileDirectory = this.LogFileDirectory,
                TempFileDirectory = this.TempFileDirectory,
                // InstallType needs to come from a drop down list
                InstallType = this.InstallType,
                EngineSvcAcc = this.engineSvcAcc,
                AgentSvcAcc = this.AgentSvcAcc,
                InstallSharedDir = this.installSharedDir,
                InstallSharedWowDir = this.installSharedWowDir,
                EngineSvcPw = this.engineSvcPw,
                AgentSvcPw = this.agentSvcPw,
                Pid = this.pid,
                FailoverClusterDisks = this.failoverClusterDisks,
                FailoverClusterGroup = this.failoverClusterGroup,
                FailoverClusterIP = this.failoverClusterIP,
                FailoverClusterName = this.failoverClusterName,
            };

            if (this.installType == "Install")
            {
                string result = template.CreateTemplate(parameters);
                this.WriteToOutputFile(result);
            }
            else if (this.installType == "InstallFailoverCluster")
            {
                //TODO: create failover install
                string result = template.InstallFailoverCluster(parameters);
                this.WriteToOutputFile(result);
            }
            else if (this.installType=="AddNode")
            {
                //TODO: create add node install
                //string result = template.AddNode(parameters)
                //this.WriteToOutputFile(result);
            }

            
        }

        private string CreateFeatureList()
        {
            const string featureSeperator = ",\"";
         
            var featureList = string.Join(featureSeperator,
                this.itemsList.Where(x => x.IsChecked).Select(x => x.DisplayText));

         
            return featureList;
        }

        private void WriteToOutputFile(string result)
        {
            var temppath = Path.GetTempPath();

            var filename = Path.Combine(temppath, "outputstring.ini");
            File.WriteAllText(filename, result);

            // Opens Explorer with file selected
            OpenFolderWithFileSelected(filename);
        }

        private void OpenFolderWithFileSelected(string filePath)
        {
            Process.Start("explorer.exe", string.Format("/select,\"{0}\"", filePath));

            ProcessStartInfo sqlCommandLineArguments = new ProcessStartInfo("cmd.exe");
            sqlCommandLineArguments.Arguments = string.Format("/K {0}\\setup.exe /QS /CONFIGURATIONFILE={1} /IACCEPTSQLSERVERLICENSETERMS", this.sqlBinariesLocation, filePath);
            Process.Start(sqlCommandLineArguments);
       
        }


    }
}