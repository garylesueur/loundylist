using System.Collections.Generic;

namespace SolidByte.LoundyList.ViewModels
{
    public class SelectableItem : ViewModel
    {
        public SelectableItem(string displayText)
        {
            this._DisplayText = displayText;
            this._IsChecked = false;
        }

        #region DisplayText :: string

        private string _DisplayText;

        public string DisplayText
        {
            get { return this._DisplayText; }
            set
            {
                if (EqualityComparer<string>.Default.Equals(this._DisplayText, value))
                {
                    return;
                }

                this._DisplayText = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region IsChecked :: bool

        private bool _IsChecked;

        public bool IsChecked
        {
            get { return this._IsChecked; }
            set
            {
                if (EqualityComparer<bool>.Default.Equals(this._IsChecked, value))
                {
                    return;
                }

                this._IsChecked = value;
                this.OnPropertyChanged();
            }
        }

        #endregion
    }
}