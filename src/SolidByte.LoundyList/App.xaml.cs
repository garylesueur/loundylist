﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using SolidByte.LoundyList.ViewModels;

namespace SolidByte.LoundyList
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var viewModel = new MainViewModel();

            var view = new MainWindow();

            view.DataContext = viewModel;

            Application.Current.MainWindow = view;

            view.Show();
        }
    }
}
