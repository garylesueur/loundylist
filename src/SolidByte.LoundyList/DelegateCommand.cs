﻿using System;
using System.Windows.Input;

namespace SolidByte.LoundyList
{
    public class DelegateCommand : ICommand
    {
        private readonly Action onExecute;

        public DelegateCommand(Action onExecute)
        {
            this.onExecute = onExecute;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (this.onExecute != null)
            {
                this.onExecute();
            }
        }

        public event EventHandler CanExecuteChanged;
    }
}