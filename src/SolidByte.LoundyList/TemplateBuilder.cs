﻿using SolidByte.LoundyList.Properties;

namespace SolidByte.LoundyList
{
    public class TemplateBuilder
    {
        public string CreateTemplate(TemplateParameters parameters)
        {
            var result = Resources.OutputTemplate;

            result = result.Replace("{FeatureList}", parameters.FeatureList);
            result = result.Replace("{EngineSvcAcc}", parameters.EngineSvcAcc);
            result = result.Replace("{EngineSvcPw}", parameters.EngineSvcPw);
            result = result.Replace("{AgentSvcAcc}", parameters.AgentSvcAcc);
            result = result.Replace("{AgentSvcPw}", parameters.AgentSvcPw);
            result = result.Replace("{DataFileDirectory}", parameters.DataFileDirectory);
            result = result.Replace("{LogFileDirectory}", parameters.LogFileDirectory);
            result = result.Replace("{TempFileDirectory}", parameters.TempFileDirectory);
            result = result.Replace("{BackupFileDirectory}", parameters.BackupFileDirectory);
            result = result.Replace("{InstanceName}", parameters.InstanceName);
            result = result.Replace("{InstallDirectory}", parameters.InstallDirectory);
            result = result.Replace("{InstallType}", parameters.InstallType);
            result = result.Replace("{InstallSharedDir}", parameters.InstallSharedDir);
            result = result.Replace("{InstallSharedWowDir}", parameters.InstallSharedWowDir);
            
            // doesn't feel like the best way to add/remove keys from the template, but works 
            if (parameters.EngineSvcPw==null)
            {
                result = result.Replace(string.Format("EngineSvcPw"), "");
            }
            else
            {
                string enginePw = string.Format("SQLSVCPASSWORD=\"{0}\"",parameters.EngineSvcPw);
                result = result.Replace("EngineSvcPw", enginePw);
            }

            if (parameters.AgentSvcPw == null)
            {
                result = result.Replace(string.Format("AgentSvcPw"), "");
            }
            else
            {
                result = result.Replace("AgentSvcPw", string.Format("AGTSVCPASSWORD=\"{0}\"", parameters.AgentSvcPw));
            }

            if (parameters.Pid == null)
            {
                result = result.Replace(string.Format("Pid"), "");
            }
            else
            {
                result = result.Replace("Pid", string.Format("Pid=\"{0}\"",parameters.Pid));
            }


            return result;
        }

        public string InstallFailoverCluster(TemplateParameters parameters)
        {
            var result = Resources.InstallFailoverCluster;

            result = result.Replace("{FeatureList}", parameters.FeatureList);
            result = result.Replace("{EngineSvcAcc}", parameters.EngineSvcAcc);
            result = result.Replace("{EngineSvcPw}", parameters.EngineSvcPw);
            result = result.Replace("{AgentSvcAcc}", parameters.AgentSvcAcc);
            result = result.Replace("{AgentSvcPw}", parameters.AgentSvcPw);
            result = result.Replace("{DataFileDirectory}", parameters.DataFileDirectory);
            result = result.Replace("{LogFileDirectory}", parameters.LogFileDirectory);
            result = result.Replace("{TempFileDirectory}", parameters.TempFileDirectory);
            result = result.Replace("{BackupFileDirectory}", parameters.BackupFileDirectory);
            result = result.Replace("{InstanceName}", parameters.InstanceName);
            result = result.Replace("{InstallDirectory}", parameters.InstallDirectory);
            result = result.Replace("{InstallType}", parameters.InstallType);
            result = result.Replace("{InstallSharedDir}", parameters.InstallSharedDir);
            result = result.Replace("{InstallSharedWowDir}", parameters.InstallSharedWowDir);
            result = result.Replace("{FailoverClusterDisks}", parameters.FailoverClusterDisks);
            result = result.Replace("{FailoverClusterIP}", parameters.FailoverClusterIP);
            result = result.Replace("{FailoverClusterGroup}", parameters.InstanceName);
            result = result.Replace("{FailoverClusterName}", parameters.FailoverClusterName);

            // doesn't feel like the best way to add/remove keys from the template, but works 
            if (parameters.EngineSvcPw == null)
            {
                result = result.Replace(string.Format("EngineSvcPw"), "");
            }
            else
            {
                string enginePw = string.Format("SQLSVCPASSWORD=\"{0}\"", parameters.EngineSvcPw);
                result = result.Replace("EngineSvcPw", enginePw);
            }

            if (parameters.AgentSvcPw == null)
            {
                result = result.Replace(string.Format("AgentSvcPw"), "");
            }
            else
            {
                result = result.Replace("AgentSvcPw", string.Format("AGTSVCPASSWORD=\"{0}\"", parameters.AgentSvcPw));
            }

            if (parameters.Pid == null)
            {
                result = result.Replace(string.Format("Pid"), "");
            }
            else
            {
                result = result.Replace("Pid", string.Format("Pid=\"{0}\"", parameters.Pid));
            }
            return result;
        }
    }
}