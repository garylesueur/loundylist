namespace SolidByte.LoundyList
{
    public class TemplateParameters
    {
        public string FeatureList { get; set; }

        public string EngineSvcAcc { get; set; }

        public string EngineSvcPw { get; set; }

        public string AgentSvcAcc { get; set; }

        public string AgentSvcPw { get; set; }

        public string DataFileDirectory { get; set; }

        public string LogFileDirectory { get; set; }

        public string TempFileDirectory { get; set; }

        public string BackupFileDirectory { get; set; }

        public string InstanceName { get; set; }

        public string InstallDirectory { get; set; }

        public string InstallType { get; set; }

        public string InstallSharedDir { get; set; }

        public string InstallSharedWowDir { get; set; }

        public string Pid { get; set; }

        public string FailoverClusterDisks { get; set; }

        public string FailoverClusterIP { get; set; }

        public string FailoverClusterGroup { get; set; }

        public string FailoverClusterName { get; set; }
        

    }
}